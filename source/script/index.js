import React from "react";
import { render } from "react-dom";
import { default as App } from "./App/App";

render(<App />, document.getElementById("app"));
