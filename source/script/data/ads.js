export default {
  AllIds: ["CLA", "STD", "PRE"],
  ById: {
    CLA: {
      title: "Classic Ad",
      desc: "Offers the most basic level of advertisement",
      price: 269.99,
      discountCodes: ["CLA_3_2", "CLA_5_4"],
    },
    STD: {
      title: "Standard Ad",
      desc:
        "Allows advertisers to use a company logo and use a longer presentation text",
      price: 322.99,
      discountCodes: ["STD_2", "STD_3"],
    },
    PRE: {
      title: "Premium Ad",
      desc:
        "Same benefits as Standout Ad, but also puts the advertisement at the top of the results, allowing higher visibility",
      price: 394.99,
      discountCodes: ["PRE_3", "PRE_4"],
    },
  },
};
