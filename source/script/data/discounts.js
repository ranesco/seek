import { default as type } from "./discountType";
export default {
  AllIds: ["CLA_3_2", "CLA_5_4", "PRE_3", "PRE_4", "STD_2", "STD_3"],
  ById: {
    CLA_3_2: {
      discountType: type.ONEFREE,
      qty: 3,
    },
    CLA_5_4: {
      discountType: type.ONEFREE,
      qty: 5,
    },
    PRE_3: {
      discountType: type.QTYXREDUCE,
      qty: 3,
      price: 389.99,
    },
    PRE_4: {
      discountType: type.QTYXREDUCE,
      qty: 4,
      price: 379.99,
    },
    STD_2: {
      discountType: type.PRICEDROP,
      qty: 1,
      price: 299.99,
    },
    STD_3: {
      discountType: type.PRICEDROP,
      qty: 1,
      price: 309.99,
    },
  },
};
