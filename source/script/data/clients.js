export default {
  AllIds: ["SEB", "ACR", "JOR", "MYR"],
  ByUserName: {
    secbite: "SEB",
    axilcr: "ACR",
    jora: "JOR",
    myer: "MYR",
  },
  ById: {
    SEB: {
      name: "SecondBite",
      username: "secbite",
      discountCodes: ["CLA_3_2"],
    },
    ACR: {
      name: "Axil Coffee Roasters",
      username: "axilcr",
      discountCodes: ["STD_2"],
    },
    JOR: {
      name: "Jora",
      username: "jora",
      discountCodes: ["PRE_4"],
    },
    MYR: {
      name: "Myer",
      username: "myer",
      discountCodes: ["CLA_5_4", "STD_3", "PRE_3"],
    },
  },
};
