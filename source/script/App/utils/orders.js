import { default as type } from "../../data/discountType";

export const calculatePrice = (price, qty) => price * qty;

export const calculateTotal = data => {
  const { userDiscounts, orders, ads, discounts } = data;
  return Object.keys(orders)
    .reduce((curr, next) => {
      return (
        (userDiscounts.length
          ? calculateDiscount({
              ads: ads,
              order: { id: next, qty: orders[next] },
              userDiscounts: userDiscounts,
              discounts: discounts,
            })
          : calculatePrice(ads.ById[next].price, orders[next])) + curr
      );
    }, 0)
    .toFixed(2);
};

const calculateDiscount = data => {
  const { userDiscounts, order, ads, discounts } = data;
  const total = userDiscounts
    .filter(discountCode =>
      ads.ById[order.id].discountCodes.includes(discountCode)
    )
    .map(discountCode => {
      const { discountType } = discounts.ById[discountCode];
      switch (discountType) {
        case type.ONEFREE:
          return calculateOneFree(data, discountCode);
        case type.QTYXREDUCE:
          return calculateReduceRate(data, discountCode);
        case type.PRICEDROP:
          return calculatePrice(discounts.ById[discountCode].price, order.qty);
        default:
          return 0;
      }
    })[0];

  return typeof total !== "undefined"
    ? total
    : calculatePrice(ads.ById[order.id].price, order.qty);
};

const calculateOneFree = (data, discountCode) => {
  const { ads, discounts, order } = data;
  const price = ads.ById[order.id].price;
  const total = calculatePrice(price, order.qty);
  return (
    total - Math.floor(order.qty / discounts.ById[discountCode].qty) * price
  );
};
const calculateReduceRate = (data, discountCode) => {
  const { ads, discounts, order } = data;
  const { qty: discountQty, price } = discounts.ById[discountCode];
  const { id, qty } = order;
  const newPrice = order.qty >= discountQty ? price : ads.ById[id].price;
  return calculatePrice(newPrice, qty);
};
