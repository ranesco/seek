import React, { Component } from "react";
import { Header, Footer, AdsList, OrderSection, LoginForm } from "./components";
import { ads, clients, discounts } from "../data";

class App extends Component {
  state = {
    user: {
      company: "",
      username: "",
      discountCodes: [],
    },
    ads: null,
    discounts: {},
    orders: {},
  };
  componentDidMount() {
    this.setState({ ads, discounts });
  }
  addToOrder = id => {
    const orders = { ...this.state.orders };
    orders[id] = orders[id] + 1 || 1;
    this.setState({ orders });
  };
  handleSubmit = username => {
    if (!username) {
      return;
    }
    const u = username.toLowerCase();

    if (clients.ByUserName.hasOwnProperty(username)) {
      const { discountCodes, name, username } = clients.ById[
        clients.ByUserName[u]
      ];
      this.setState({
        user: {
          company: name,
          discountCodes,
          username,
        },
      });
    }
  };

  handleLogout = () => {
    this.setState({
      user: {
        company: "",
        username: "",
        discountCodes: [],
      },
      orders: {},
    });
  };

  render() {
    const { ads, orders, discounts, user } = this.state;
    return (
      <div>
        <Header />
        <LoginForm
          handleSubmit={this.handleSubmit}
          handleLogout={this.handleLogout}
          user={user}
        />
        {ads ? (
          <div className="body">
            <section>
              <h2>Available Ads</h2>
              <AdsList addToOrder={this.addToOrder} ads={ads} />
            </section>
            <OrderSection
              ads={ads}
              orders={orders}
              discounts={discounts}
              userDiscounts={user.discountCodes}
            />
          </div>
        ) : (
          "Loading"
        )}
        <Footer />
      </div>
    );
  }
}

// <OrderSection
// displayAds={displayAds}
// orders={orders}
// prices={prices}
// discounts={user.discountCodes}
// />

export default App;
