import React from "react";

const Header = () => (
  <header>
    <h1>SEEK</h1>
    <hr />
  </header>
);

export default Header;
