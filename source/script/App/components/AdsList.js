import React from "react";
import PropTypes from "prop-types";
import Ad from "./Ad";

const AdsList = ({ ads, addToOrder }) => {
  return (
    <div>
      {ads.AllIds.map(ad => {
        return (
          <Ad
            key={ad}
            addToOrder={addToOrder}
            detail={{
              id: ad,
              ...ads.ById[ad],
            }}
          />
        );
      })}
    </div>
  );
};

AdsList.propTypes = {
  ads: PropTypes.shape({}).isRequired,
  addToOrder: PropTypes.func.isRequired,
};

export default AdsList;
