import React from "react";
import PropTypes from "prop-types";

const Order = ({ count, title, price }) => {
  return (
    <li>
      <p>
        {count} x {title}
      </p>
      <p>${price} ea.</p>
    </li>
  );
};
Order.propTypes = {
  count: PropTypes.number,
  title: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
};

export default Order;
