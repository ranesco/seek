import React from "react";
import PropTypes from "prop-types";
import { isEmptyObject } from "../utils/objects";
import OrdersList from "./OrdersList";
import OrderTotal from "./OrderTotal";

const OrderSection = ({ orders, ads, discounts, userDiscounts }) => {
  return (
    <section>
      <h2>Ordered List</h2>
      <OrdersList orders={orders} ads={ads} />
      <OrderTotal
        ads={ads}
        orders={orders}
        userDiscounts={userDiscounts}
        discounts={discounts}
      />
    </section>
  );
};

OrderSection.propTypes = {
  orders: PropTypes.shape({}).isRequired,
  ads: PropTypes.shape({}).isRequired,
  discounts: PropTypes.shape({}).isRequired,
  userDiscounts: PropTypes.array.isRequired,
};

export default OrderSection;
