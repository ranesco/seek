import React from "react";
import PropTypes from "prop-types";
import { isEmptyObject } from "../utils/objects";
import Order from "./Order";

const OrdersList = ({ orders, ads }) => {
  return (
    <div>
      {!isEmptyObject(orders) ? (
        <ul>
          {Object.keys(orders).map(id => {
            const { price, title } = ads.ById[id];
            const order = {
              count: orders[id],
              title,
              price,
            };
            return <Order key={id} {...order} />;
          })}
        </ul>
      ) : (
        <p>Please choose one of the display ads</p>
      )}
    </div>
  );
};

OrdersList.propTypes = {
  orders: PropTypes.shape({}).isRequired,
  ads: PropTypes.shape({}).isRequired,
};

export default OrdersList;
