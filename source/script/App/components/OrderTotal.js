import React from "react";
import PropTypes from "prop-types";
import { isEmptyObject } from "../utils/objects";
import { calculatePrice, calculateTotal } from "../utils/orders";

const OrderTotal = ({ ads, orders, userDiscounts, discounts }) => {
  const data = { ads, orders, userDiscounts, discounts };
  return <div>Total ${calculateTotal(data)}</div>;
};

OrderTotal.propTypes = {
  ads: PropTypes.shape({}).isRequired,
  orders: PropTypes.shape({}).isRequired,
  discounts: PropTypes.shape({}).isRequired,
  userDiscounts: PropTypes.array.isRequired,
};

export default OrderTotal;
