import React, { Component } from "react";
import PropTypes from "prop-types";

class LoginForm extends Component {
  static defaultProps = {
    handleSubmit() {},
    handleLogout() {},
    username: "",
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.handleSubmit(this.input.value);
    this.loginForm.reset();
  };
  handleLogout = e => {
    e.preventDefault();
    this.props.handleLogout();
  };
  render() {
    const { user } = this.props;
    const el = user.company ? (
      <p>
        Welcome {user.company}!{" "}
        <button onClick={this.handleLogout}>Logout</button>
      </p>
    ) : (
      <form ref={form => (this.loginForm = form)} onSubmit={this.handleSubmit}>
        <input
          ref={input => (this.input = input)}
          type="text"
          placeholder="Username"
        />
        <button type="submit">Login</button>
      </form>
    );
    return el;
  }
}

LoginForm.PropTypes = {
  handleSubmit: PropTypes.func.isRequired,
  handleLogout: PropTypes.func.isRequired,
  user: PropTypes.shape({}).isRequired,
};

export default LoginForm;
