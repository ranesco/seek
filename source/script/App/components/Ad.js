import React from "react";
import PropTypes from "prop-types";

const Ad = ({ detail, addToOrder }) => {
  const { id, title, desc, price } = detail;
  return (
    <article>
      <h3>{title}</h3>
      <p>{desc}</p>
      <button onClick={() => addToOrder(id)}>${price}</button>
    </article>
  );
};
Ad.propTypes = {
  detail: PropTypes.shape({
    title: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    price: PropTypes.number,
    id: PropTypes.string.isRequired,
  }),
  addToOrder: PropTypes.func,
};

export default Ad;
